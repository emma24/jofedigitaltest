@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">

                    @if(Auth::user()->is_admin == 1)

                    @else
                    <div class="card">
                        <div class="card-body">
                              <a class="nav-link" href="{{ url('productsUser')  }}">{{ __('Les Produits') }}</a>
                       </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
