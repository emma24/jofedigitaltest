@extends('products.layout')

@section('content')
    <div class="row pt-5 mt-5">
        <div class="col-lg-12 margin-tb pt-5 mt-5">
            <div class="pull-left pt-5 mt-5" style="margin-top:5%">
                <h2>La liste de ventes d'article</h2>
            </div>
            <div class="pull-right" style="margin-top:5%">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Ajouter un produit</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="card">

    <table class="table table-bordered " style="margin-top:5%">
        <tr>
            <th>No</th>
            <th>Nom</th>
            <th>Détails</th>
            <th width="380px">Action</th>
        </tr>
        @foreach ($products as $product)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->name }} <img src="/storage/images/{{$product->image}}" style="height:100px; width:100px" ></td>
            <td>{{ $product->detail}}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Vue</a>

                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Modifier</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

    {!! $products->links() !!}

@endsection
