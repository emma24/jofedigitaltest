@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb" style="margin-top:5%">
            <div class="pull-left">
                <h2> Voire Produit</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Arrière</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nom:</strong>
                {{ $product->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Détails:</strong>
                {{ $product->detail }}
            </div>
        </div>
    </div>
@endsection
