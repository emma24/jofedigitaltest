@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if(Auth::user()->is_admin == 1)
                    <div class="card">
                        <div class="card-body">
                              <a class="nav-link" href="{{ url('products')  }}">{{ __('Les Produits') }}</a>
                       </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                              <a class="nav-link" href="{{ url('categories')  }}">{{ __('Les Categories') }}</a>
                       </div>
                    </div>

                    @else

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
