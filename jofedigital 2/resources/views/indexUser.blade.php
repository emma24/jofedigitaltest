@extends('categories.layout')

@section('content')
    <div class="row pt-5 mt-5">
        <div class="col-lg-12 margin-tb pt-5 mt-5">
            <div class="pull-left pt-5 mt-5" style="margin-top:5%">
                <h2>La liste des categories de ventes d'article</h2>
            </div>
            <div class="pull-right" style="margin-top:5%">
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Ajouter une nouvelle categorie </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered " style="margin-top:5%">
        <tr>
            <th>No</th>
            <th>Categorie</th>
            <th>Categorie</th>
            <th width="380px">Action</th>
        </tr>
        @foreach ($categories as $category)
        @foreach ($products as $product)

        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $category->id }}</td>
            <td>{{ $product->id }}</td>
            <td>
            </td>
        </tr>
        @endforeach

        @endforeach
    </table>

    {!! $categories->links() !!}

@endsection
