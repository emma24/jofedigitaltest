@extends('categories.layout')

@section('content')
    <div class="row pt-5 mt-5">
        <div class="col-lg-12 margin-tb pt-5 mt-5">
            <div class="pull-left pt-5 mt-5" style="margin-top:5%">
                <h2>La liste des categories de ventes d'article</h2>
            </div>
            <div class="pull-right" style="margin-top:5%">
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Ajouter une nouvelle categorie </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered " style="margin-top:5%">
        <tr>
            <th>No</th>
            <th>Categorie</th>
            <th width="380px">Action</th>
        </tr>
        @foreach ($categories as $category)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $category->name }}</td>
            <td>
                <form action="{{ route('categories.destroy',$category->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('categories.show',$category->id) }}">Vue</a>

                    <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">Modifier</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

    {!! $categories->links() !!}

@endsection
