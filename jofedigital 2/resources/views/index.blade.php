@extends('products.layout')

@section('content')
    <div class="row pt-5 mt-5">
        <div class="col-lg-12 margin-tb pt-5 mt-5">
            <div class="pull-left pt-5 mt-5" style="margin-top:5%">
                <h2>La liste de ventes d'article</h2>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered " style="margin-top:5%">
        <tr>
            <th>No</th>
            <th>Nom</th>
            <th>Détails</th>
            <!-- <th>Category</th> -->
        </tr>
        @foreach ($products as $product)

        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->name }} <img src="/storage/images/{{$product->image}}" style="height:100px; width:100px"> </td>
            <td>{{ $product->detail }}</td>
            <!-- <td>{{ $product->category }}</td> -->


        </tr>

        @endforeach
    </table>

    {!! $products->links() !!}

@endsection
