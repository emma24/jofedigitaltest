<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::latest()->paginate(5);
      $products = Product::latest()->paginate(5);

      return view('indexUser',compact('categories','products'))
        ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoryUser  $categoryUser
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryUser $categoryUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategoryUser  $categoryUser
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryUser $categoryUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategoryUser  $categoryUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryUser $categoryUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoryUser  $categoryUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryUser $categoryUser)
    {
        //
    }
}
